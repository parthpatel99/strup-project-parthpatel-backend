const express = require('express');
const Albums = require('../models/albums');

const router = express.Router();

router.get('/', (req, res) => {
  Albums.find().then(documents => res.json(documents)).catch(error => res.send(error));
});

router.post('/', (req, res) => {
  console.log(req.body);
  const model = new Albums({
    ...req.body,
  });

  model
    .save()
    .then(documents => {
      res.json(documents);
    })
    .catch(err => {
      console.log(err);
      res.send(err);
    } );
});

router.patch('/:id', (req, res) => {
  const setList = ['title', 'url', 'thumbnailUrl'];
  const update = {};

  Object.keys(req.body).forEach(key => {
    if (setList.includes(key)) {
      if (!update.$set) update.$set = {};
      update.$set[key] = req.body[key];
    }
  });

  if (Object.keys(update).length === 0) {
    next(new Error('no body data'));
    return;
  }

  Albums.updateOne({ _id: req.params.id }, update, { multi: false, upsert: false })
    .then(documents => {
      res.json(documents);
    })
    .catch(err => next(err));
});

router.delete('/:id', (req, res) => {
  console.log(req.params.id);
  Albums.findOne({ _id: req.params.id }).lean().then(document => {
    if (document) {
      Albums.deleteOne({ _id: req.params.id }).then(_ => {
        res.json({ status: 'Deleted' });
      }).catch(error => {
        console.log(error);
        res.status(400).json({ error }); 
      });
    } else {
      res.status(404).json({ error: 'Document not found' });
    }
  }).catch(error => res.status(400).json({ error }));
});

module.exports = router;