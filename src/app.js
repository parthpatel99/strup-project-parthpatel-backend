const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use('/albums', require("./routes/albums"));

app.get("/", (req, res) => {
  res.status(200).json({ "status": "You are on parth patel's laptop" });
});

mongoose.connect(
  'mongodb+srv://nodeuser:node12345@parthcluster0.lrr4j.mongodb.net/upstreet_albums',
  { useNewUrlParser: true, useUnifiedTopology: true },
  (err) => {
    console.log("Connected to mongodb" + err);
  })

app.listen(process.env.PORT || 5000, () => console.log("Started Running"));