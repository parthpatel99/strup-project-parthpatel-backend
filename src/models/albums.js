const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;

const AlbumsSchema = Schema({
    title: String,
    url: String,
    thumbnailUrl: String,
});

module.exports = mongoose.model("Albums", AlbumsSchema);
